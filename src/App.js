import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";

import ProgressBarUI from "./components/progressbar";

class App extends Component {

  constructor(){
    super();
    this.state ={
      temp : -1,
      humidity: -1,
      tempColor: '',
      tempText: '',
      humidityColor: ''
    }
  }

  componentDidMount(){
    setInterval(() => {
      fetch('http://172.20.10.6:3031/datas')
        .then(res => res.json())
        .then(json => {
          this.setState({
            temp: parseInt(json.data.temperature),
            humidity: parseInt(json.data.humidity) 
          })
          this.colorChooseTemp(parseInt(json.data.temperature))
          this.colorChooseHumidity(parseInt(json.data.humidity))
        }, 1000);
    })

  }

  colorChooseTemp(temp){
    let tmpColor = 'red';
    let tempText = 'Clim activated !';
    if (temp < 31 && temp >= 28){
      tmpColor = 'orange';
      tempText = 'Warning !';
    }else if (temp < 28 && temp > 21){
      tmpColor = 'green';
      tempText = 'Good !';
    }else if (temp <= 21 && temp > 18){
      tmpColor = 'orange';
      tempText = 'Warning !';
    }else if(temp < 18){
      tempText = 'Radiator activated !'
    }
    this.setState({tempColor: tmpColor, tempText: tempText})
  }

  colorChooseHumidity(humidity){
    let tmpColor = 'red';
    if (humidity < 90 && humidity >= 85){
      tmpColor = 'orange';
    }else if (humidity < 85 && humidity > 70){
      tmpColor = 'green';
    }else if (humidity <= 70 && humidity > 60){
      tmpColor = 'orange';
    }
    this.setState({humidityColor: tmpColor})
  }
  
  render() {
    return (
      <div>
        <div className="App">
          <ProgressBarUI text={this.state.tempText} color={this.state.tempColor} title="Temperature" symType='°C' maxValue="40" percentage={this.state.temp} />
          <ProgressBarUI color={this.state.humidityColor} title="Humidity" symType='%' maxValue="100" percentage={this.state.humidity} />
        </div>
      </div>
    );
  }
}

export default App;
