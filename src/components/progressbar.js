import React, { Component } from "react";
import "./progressbar.css";
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

export default class ProgressBarUI extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <div className='sizeAll'>
          <h1>{this.props.title}</h1>
        <CircularProgressbar className={this.props.color} value={this.props.percentage} maxValue={this.props.maxValue} text={`${this.props.percentage} ${this.props.symType}`} />
        <p className={this.props.color}>{this.props.text}</p>
      </div>
    );
  }
}
